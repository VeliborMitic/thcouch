import asyncio
import pytest

from thcouch.core.server import (
    GetServerOk, GetServerErr, GetServerResult, get_server,
    GetAllDbsOk, GetAllDbsErr, GetAllDbsResult, get_all_dbs,
)
# from thcouch.core.server.unwrap import get_server, get_all_dbs

from thcouch.core.db import (
    PutDbResult, PutDbOk, PutDbErr, put_db,
    DeleteDbResult, DeleteDbOk, DeleteDbErr, delete_db,
)
# from thcouch.core.db.unwrap import put_db, delete_db

COUCH_URI = 'http://tangledhub:tangledhub@couchdb-test:5984'


@pytest.fixture(scope='session')
def event_loop(request):
    """Create an instance of the default event loop for each test case."""
    loop = asyncio.get_event_loop_policy().new_event_loop()
    yield loop
    loop.close()


@pytest.fixture(scope='session', autouse=True)
@pytest.mark.asyncio
async def couch_session_teardown(request):
    all_dbs = (await get_all_dbs(uri=COUCH_URI)).unwrap()
   
    for db in all_dbs.dbs:
        (await delete_db(uri=COUCH_URI, db=db)).unwrap()
    
    yield


"""
@pytest.fixture
@pytest.mark.asyncio
async def couch_setup_teardown(request):
    name = request.function.__name__
    ts = datetime.utcnow().isoformat().replace(':', '').replace('.', '').replace('-', '').replace('T', '_').lower()
    COUCH_DATABASE = f'test_{name}_{ts}'
    COUCH_DATABASE_URL = f'{COUCH_URI}/{COUCH_DATABASE}'

    # check if couch server is running
    while True:
        match obj := await get_server(COUCH_URI):
            case GetServerOk():
                break
            case _:
                await asyncio.sleep(1.0)

    # create database
    match await put_db(uri=COUCH_URI, db=COUCH_DATABASE):
        case PutDbOk(ok):
            pass
        case PutDbErr(error, reason):
            raise Exception(f'Could not create database, error: {error!r}, reason: {reason!r}')
        case _:
            raise Exception('Could not create database, server not responding')

    # run individual test
    yield COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL

    # delete database
    match await delete_db(uri=COUCH_URI, db=COUCH_DATABASE):
        case DeleteDbOk(ok):
            pass
        case DeleteDbErr(error, reason):
            raise Exception(f'Could not delete database, error: {error!r}, reason: {reason!r}')
        case _:
            raise Exception('Could not delete database, server not responding')
"""


@pytest.fixture
@pytest.mark.asyncio
async def couch_setup(request):
    name = request.function.__name__
    # ts = datetime.utcnow().isoformat().replace(':', '').replace('.', '').replace('-', '').replace('T', '_').lower()
    # COUCH_DATABASE = f'{name}_{ts}'
    COUCH_DATABASE = name
    COUCH_DATABASE_URL = f'{COUCH_URI}/{COUCH_DATABASE}'
    
    # check if couch server is running
    while True:
        get_server_ = (await get_server(COUCH_URI)).unwrap()
        break

    put_db_ = (await put_db(uri=COUCH_URI, db=COUCH_DATABASE)).unwrap()

    # run individual test
    yield COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL
    