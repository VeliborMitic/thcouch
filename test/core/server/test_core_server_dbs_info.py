import pytest

from thresult import ResultException

from thcouch.core.db import PutDbOk, DeleteDbOk, put_db, delete_db
from thcouch.core.server import GetDbsInfoOk, post_dbs_info


@pytest.mark.asyncio
async def test_core_server_dbs_info(couch_setup):
    '''
    Returns information of a list of the specified databases in the CouchDB instance - success.
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    dbs_to_create = ['dbs_info_test_db1', 'dbs_info_test_db2', 'dbs_info_test_db3']
    for db in dbs_to_create:
        put_db_res = (await put_db(COUCH_URI, db)).unwrap()
        assert isinstance(put_db_res, PutDbOk)

    post_dbs_info_res = (await post_dbs_info(uri=COUCH_URI, keys=dbs_to_create)).unwrap()
    assert isinstance(post_dbs_info_res, GetDbsInfoOk)

    # remove created databases
    for db in dbs_to_create:
        delete_db_res = (await delete_db(COUCH_URI, db)).unwrap()
        assert isinstance(delete_db_res, DeleteDbOk)


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException)
async def test_core_server_dbs_info_missing_key(couch_setup):
    '''
    keys (array) – Array of database names to be requested.
    Keys are set to None, instead to an array of db names - expected exception
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    keys = None

    post_dbs_info_res = (await post_dbs_info(uri=COUCH_URI, keys=keys)).unwrap()


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException)
async def test_core_server_dbs_info_wrong_uri(couch_setup):
    '''
    Returns information of a list of the specified databases in the CouchDB instance - expected exception
    Wrong uri given
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    dbs_to_create = ['dbs_info_test_db1', 'dbs_info_test_db2', 'dbs_info_test_db3']
    for db in dbs_to_create:
        put_db_res = (await put_db(COUCH_URI, db)).unwrap()

    post_dbs_info_res = (await post_dbs_info(uri='COUCH_URI', keys='dbs_to_create')).unwrap()
