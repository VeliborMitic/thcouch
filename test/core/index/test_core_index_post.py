import pytest

from thresult import ResultException

from thcouch.core.index import IndexOk, post_index


@pytest.mark.asyncio
async def test_core_index_post(couch_setup):
    '''
    This function tests post index function for creating a new index - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    res = (await post_index(uri=COUCH_URI,
                            db=COUCH_DATABASE,
                            index={'fields': ['collection', 'test_index']})).unwrap()

    assert isinstance(res, IndexOk)


@pytest.mark.asyncio
async def test_core_index_post_name(couch_setup):
    '''
    This function tests post index function for creating a new index passing name param - success
    '''    
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    res = (await post_index(uri=COUCH_URI,
                            db=COUCH_DATABASE,
                            index={'fields': ['collection', 'test_index']},
                            name='test')).unwrap()

    assert isinstance(res, IndexOk)


@pytest.mark.asyncio
async def test_core_index_post_ddoc(couch_setup):
    '''
    This function tests post index function for creating a new index passing ddoc param - succes
    '''    
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    res = (await post_index(uri=COUCH_URI,
                            db=COUCH_DATABASE,
                            index={'fields': ['collection', 'test_index']},
                            ddoc='test_id')).unwrap()

    assert isinstance(res, IndexOk)


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException)
async def test_core_index_post_fails_wrong_url(couch_setup):
    '''
    This function tests get index function when wrong uri passed - exception expected
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    res = (await post_index(uri='COUCH_URI', 
                            db=COUCH_DATABASE,
                            index={'fields': ['collection', 'test_index']})).unwrap()


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException)
async def test_core_index_post_fails_wrong_partial_filter_selector(couch_setup):
    '''
    This function tests post index function for creating a new index
    when wrong partial filter selector passed - exception expected
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    res = (await post_index(uri=COUCH_URI,
                            db=COUCH_DATABASE,
                            partial_filter_selector={"error": "error"},
                            index={'fields': ['collection', 'test_index']})).unwrap()
                                  

@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException)
async def test_core_index_post_fails_wrong_partitioned_given_value_true(couch_setup):
    '''
    This function tests post index function for creating a new index
    when wrong partitioned param passed - exception expected
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    res = (await post_index(uri=COUCH_URI,
                            db=COUCH_DATABASE,
                            partitioned=True,
                            index={'fields': ['collection', 'test_index']})).unwrap()


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException)
async def test_core_index_post_fails_wrong_db(couch_setup):
    '''
    This function tests post index function for creating a new index
    when wrong db param passed - exception expected
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    res = (await post_index(uri=COUCH_URI,
                            db="COUCH_DATABASE",
                            index={'fields': ['collection', 'test_index']})).unwrap()
