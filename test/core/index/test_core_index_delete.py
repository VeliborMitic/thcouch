import pytest

from thresult import ResultException

from thcouch.core.index import DeleteIndexOk, post_index, delete_index


@pytest.mark.asyncio
async def test_core_index_delete(couch_setup):
    '''
    This function tests delete index - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    # called post_index function to create the index for delete_index function test
    res = (await post_index(uri=COUCH_URI,
                            db=COUCH_DATABASE,
                            ddoc='uuid',
                            name='test-index',
                            index={'fields': ['collection', 'test_index']})).unwrap()

    delete_res = (await delete_index(uri=COUCH_URI,
                                     db=COUCH_DATABASE,
                                     name='test-index',
                                     designdoc='uuid')).unwrap()

    assert isinstance(delete_res, DeleteIndexOk)


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException)
async def test_core_index_delete_fails_wrong_name(couch_setup):
    '''
    This function tests delete index when wrong name passed - exception expected
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    res = (await delete_index(uri=COUCH_URI,
                              db=COUCH_DATABASE,
                              name='nonexistent-index-name',
                              designdoc='uuid')).unwrap()


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException)
async def test_core_index_delete_fails_wrong_uri(couch_setup):
    '''
    This function tests delete index when wrong uri passed - exception expected
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    res = (await delete_index(uri='COUCH_URI',
                             db=COUCH_DATABASE,
                             name='test-index',
                             designdoc='uuid')).unwrap()


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException)
async def test_core_index_delete_fails_wrong_name_invalid(couch_setup):
    '''
    This function tests delete index when wrong or ivalid name passed - exception expected
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    res = (await delete_index(uri=COUCH_URI,
                              db=COUCH_DATABASE,
                              name='test-index/error/error/error',
                              designdoc='uuid')).unwrap()
