import pytest

from thresult import ResultException

from thcouch.core.doc import GetDocOk, put_doc, get_doc
from thcouch.core.attachment import put_attachment
from thcouch.core.db import post_db


@pytest.mark.asyncio
async def test_get_doc(couch_setup):
    '''
    This function tests get doc function - expect success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    doc = (await put_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid='0', doc={'x': 10})).unwrap()
    doc = (await get_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid='0')).unwrap()
    assert isinstance(doc, GetDocOk)


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException)
async def test_get_doc_wrong_uri(couch_setup):
    '''
    This function tests get doc function with wrong uri - expect exception
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    doc = (await get_doc(uri='COUCH_URI', db=COUCH_DATABASE, docid='0')).unwrap()


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException)
async def test_get_doc_not_found(couch_setup):
    '''
    This function tests get doc function,
    404 Not Found – Document not found - expect exception
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    doc = (await put_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid='0', doc={'x': 10})).unwrap()

    wrong_id: str = doc.id + '17'
    doc = (await get_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid=wrong_id)).unwrap()


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException)
async def test_get_doc_empty_string_userid_passed(couch_setup):
    '''
    This function tests get doc function,
    404 Not Found – Document not found - expect exception
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    doc = (await get_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid='')).unwrap()


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException)
async def test_get_doc_empty_string_db_passed(couch_setup):
    '''
    This function tests get doc function,
    404 Not Found – Document not found - expect exception
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    doc = (await put_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid='0', doc={'x': 10})).unwrap()

    doc_ = (await get_doc(uri=COUCH_URI, db='', docid=doc.id)).unwrap()


# FIXME:
@pytest.mark.skip(reason='Call to Couchdb raises Exception!')
@pytest.mark.asyncio
async def test_get_doc_attachments(couch_setup):
    '''
    This function tests attachments
    attachments (boolean) – Includes attachments bodies in response - expect success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    doc={'x': 10}
    doc = (await post_db(uri=COUCH_URI, db=COUCH_DATABASE, doc=doc)).unwrap()

    file_name = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()

    id: str = doc.id
    rev: str = doc.rev
    att = (await put_attachment(uri=COUCH_URI,
                                db=COUCH_DATABASE,
                                docid=id,
                                attachment_name=file_name,
                                rev=rev,
                                body=content)).unwrap()
    
    doc_dict = (await get_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid=id, attachments=True)).unwrap()
    doc = doc_dict.doc
    assert '_attachments' in doc


# FIXME: att_encoding_info=True does not produce any efect
@pytest.mark.asyncio
async def test_get_doc_att_encoding_info(couch_setup):
    '''
    This function tests attachments
    att_encoding_info (boolean) – 
    Includes encoding information in attachment stubs 
    if the particular attachment is compressed. Default is false - expect success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    doc={'x': 10}
    doc = (await post_db(uri=COUCH_URI, db=COUCH_DATABASE, doc=doc)).unwrap()

    file_name = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()

    id: str = doc.id
    rev: str = doc.rev
    att = (await put_attachment(uri=COUCH_URI, db=COUCH_DATABASE, docid=id, attachment_name=file_name, rev=rev, body=content)).unwrap()
    
    doc_dict = (await get_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid=id, att_encoding_info=True)).unwrap()
    doc = doc_dict.doc
    assert 'stub' in doc['_attachments'][file_name]


# FIXME: ContentTypeError, message:'Attempt to decode JSON with unexpected mimetype: multipart/related';
@pytest.mark.asyncio
async def test_get_doc_atts_since(couch_setup):
    '''
    This function tests get doc function,
    Includes attachments only since specified revisions - expect success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    doc={'x': 10}
    doc = (await post_db(uri=COUCH_URI, db=COUCH_DATABASE, doc=doc)).unwrap()

    file_name = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()

    id: str = doc.id
    rev: str = doc.rev
    att = (await put_attachment(uri=COUCH_URI, db=COUCH_DATABASE, docid=id, attachment_name=file_name, rev=rev, body=content)).unwrap()
    
    # get rev when attachment was added
    doc_dict = (await get_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid=id)).unwrap()
    doc = doc_dict.doc
    rev = doc['_rev']
    doc_dict = (await get_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid=id, atts_since=[rev])).unwrap()
    assert isinstance(doc_dict, GetDocOk)


@pytest.mark.asyncio
async def test_get_doc_conflicts(couch_setup):
    '''
    This function tests get doc function,
    conflicts (boolean) – Includes information about conflicts in document. 
    Default is false - expect success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    doc={'x': 10}
    doc = (await post_db(uri=COUCH_URI, db=COUCH_DATABASE, doc=doc)).unwrap()
    id: str = doc.id
    doc_dict = (await get_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid=id, conflicts=True)).unwrap()
    assert isinstance(doc_dict, GetDocOk)


@pytest.mark.asyncio
async def test_get_doc_deleted_conflicts(couch_setup):
    '''
    This function tests get doc function,
    deleted_conflicts (boolean) – Includes information about deleted conflicted revisions. 
    Default is false - expect success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    doc={'x': 10}
    doc = (await post_db(uri=COUCH_URI, db=COUCH_DATABASE, doc=doc)).unwrap()
    id: str = doc.id
    id: str = doc.id
    doc_dict = (await get_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid=id, deleted_conflicts=True)).unwrap()
    assert isinstance(doc_dict, GetDocOk)


@pytest.mark.asyncio
async def test_get_doc_latest(couch_setup):
    '''
    This function tests get doc function,
    latest (boolean) – Forces retrieving latest “leaf” revision, 
    no matter what rev was requested. Default is false - expect success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    doc={'x': 10}
    doc = (await post_db(uri=COUCH_URI, db=COUCH_DATABASE, doc=doc)).unwrap()

    file_name = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()

    id: str = doc.id
    rev: str = doc.rev
    att = (await put_attachment(uri=COUCH_URI, db=COUCH_DATABASE, docid=id, attachment_name=file_name, rev=rev, body=content)).unwrap()
    
    old_rev = rev # rev has changed, attachment was added
    doc_dict = (await get_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid=id, rev=old_rev, latest=True)).unwrap()
    doc = doc_dict.doc
    assert old_rev != doc['_rev']


@pytest.mark.asyncio
async def test_get_doc_local_seq(couch_setup):
    '''
    This function tests get doc function,
    local_seq (boolean) – Includes last update sequence for the document. Default is false - expect success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    doc={'x': 10}
    doc = (await post_db(uri=COUCH_URI, db=COUCH_DATABASE, doc=doc)).unwrap()

    file_name = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()

    id: str = doc.id
    rev: str = doc.rev
    att = (await put_attachment(uri=COUCH_URI, db=COUCH_DATABASE, docid=id, attachment_name=file_name, rev=rev, body=content)).unwrap()
    
    doc_dict = (await get_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid=id, local_seq=True)).unwrap()
    doc = doc_dict.doc
    assert '_local_seq' in doc


@pytest.mark.asyncio
async def test_get_doc_meta(couch_setup):
    '''
    This function tests get doc function,
    meta (boolean) – Acts same as specifying all conflicts, 
    deleted_conflicts and revs_info query parameters. Default is false - expect success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    doc={'x': 10}
    doc = (await post_db(uri=COUCH_URI, db=COUCH_DATABASE, doc=doc)).unwrap()

    file_name = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()

    id: str = doc.id
    rev: str = doc.rev
    att = (await put_attachment(uri=COUCH_URI, db=COUCH_DATABASE, docid=id, attachment_name=file_name, rev=rev, body=content)).unwrap()
    
    doc_dict = (await get_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid=id, meta=True)).unwrap()
    doc = doc_dict.doc
    assert '_revs_info' in doc


@pytest.mark.asyncio
async def test_get_doc_open_revs(couch_setup):
    '''
    This function tests get doc function,
    Retrieves documents of specified leaf revisions - expect success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    doc = (await put_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid='0', doc={'x': 10})).unwrap()
    id: str = doc.id
    doc_dict = (await get_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid=id, open_revs=[])).unwrap()
    assert isinstance(doc_dict, GetDocOk)


@pytest.mark.asyncio
async def test_get_doc_rev(couch_setup):
    '''
    This function tests get doc function,
    Retrieves document of specified revision - expect success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    doc={'x': 10}
    doc = (await post_db(uri=COUCH_URI, db=COUCH_DATABASE, doc=doc)).unwrap()

    file_name = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()

    id: str = doc.id
    rev: str = doc.rev
    att = (await put_attachment(uri=COUCH_URI, db=COUCH_DATABASE, docid=id, attachment_name=file_name, rev=rev, body=content)).unwrap()
    
    old_rev = rev # rev has changed, attachment was added
    doc_dict = (await get_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid=id, rev=old_rev)).unwrap()
    doc = doc_dict.doc
    assert old_rev == doc['_rev']


@pytest.mark.asyncio
async def test_get_doc_revs(couch_setup):
    '''
    This function tests get doc function,
    revs (boolean) – Includes list of all known document revisions. Default is false - expect success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    doc={'x': 10}
    doc = (await post_db(uri=COUCH_URI, db=COUCH_DATABASE, doc=doc)).unwrap()

    file_name = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()

    id: str = doc.id
    rev: str = doc.rev
    att = (await put_attachment(uri=COUCH_URI, db=COUCH_DATABASE, docid=id, attachment_name=file_name, rev=rev, body=content)).unwrap()
    
    doc_dict = (await get_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid=id, revs=True)).unwrap()
    doc = doc_dict.doc
    assert '_revisions' in doc


@pytest.mark.asyncio
async def test_get_doc_revs_info(couch_setup):
    '''
    This function tests get doc function,
    revs_info (boolean) – Includes detailed information for all known document revisions. 
    Default is false - expect success 
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    doc={'x': 10}
    doc = (await post_db(uri=COUCH_URI, db=COUCH_DATABASE, doc=doc)).unwrap()

    file_name = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()

    id: str = doc.id
    rev: str = doc.rev
    att = (await put_attachment(uri=COUCH_URI, db=COUCH_DATABASE, docid=id, attachment_name=file_name, rev=rev, body=content)).unwrap()
    
    doc_dict = (await get_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid=id, revs_info=True)).unwrap()
    doc = doc_dict.doc
    assert '_revs_info' in doc
