import pytest

from thresult import ResultException

from thcouch.core.doc import HeadDocOk, put_doc, head_doc


@pytest.mark.asyncio
async def test_head_doc(couch_setup):
    '''
    This function tests head doc function - expect success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    doc = (await put_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid='0', doc={'x': 10})).unwrap()
    res = (await head_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid='0')).unwrap()
    assert isinstance(res, HeadDocOk)


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException)
async def test_head_doc_db_param_empty_string(couch_setup):
    '''
    This function tests head doc function - expect success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    doc = (await put_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid='0', doc={'x': 10})).unwrap()
    res = (await head_doc(uri=COUCH_URI, db='', docid='0')).unwrap()
    print(res)
    assert isinstance(res, HeadDocOk)


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException)
async def test_head_doc_docid_param_empty_string(couch_setup):
    '''
    This function tests head doc function - expect success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    doc = (await put_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid='0', doc={'x': 10})).unwrap()
    res = (await head_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid='')).unwrap()
    print(res)
    assert isinstance(res, HeadDocOk)


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException)
async def test_head_doc_wrong_uri(couch_setup):
    '''
    This function tests head doc function, wrong uri - expect exception
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    doc = (await put_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid='0', doc={'x': 10})).unwrap()
    res = (await head_doc(uri='COUCH_URI', db=COUCH_DATABASE, docid='0')).unwrap()


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException)
async def test_head_doc_wrong_db(couch_setup):
    '''
    This function tests head doc function, wrong db - expect exception
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    doc = (await put_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid='0', doc={'x': 10})).unwrap()
    res = (await head_doc(uri=COUCH_URI, db='COUCH_DATABASE', docid='0')).unwrap()


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException)
async def test_head_doc_not_found(couch_setup):
    '''
    This function tests head doc function,
    404 Not Found – Document not found, wrong id - expect exception
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    doc = (await put_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid='0', doc={'x': 10})).unwrap()
    res = (await head_doc(uri=COUCH_URI, db=COUCH_DATABASE, docid='12')).unwrap()
