import json
import pytest

from thresult import ResultException

from thcouch.core.db import BulkGetOk, bulk_get, post_db


@pytest.mark.asyncio
async def test_core_db_bulk_get(couch_setup):
    '''
    This function tests bulk get function - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    # create doc1
    doc1_dict = {'a': 10}
    doc1 = (await post_db(uri=COUCH_URI, db=COUCH_DATABASE, doc=doc1_dict)).unwrap()

    # create doc2
    doc2_dict = {'b': 20}
    doc2 = (await post_db(uri=COUCH_URI, db=COUCH_DATABASE, doc=doc2_dict)).unwrap()

    # test bulk_get
    docs = (await bulk_get(uri=COUCH_URI, db=COUCH_DATABASE, docs=[{"id": doc1.id}, {"id": doc2.id}])).unwrap()
    assert isinstance(docs, BulkGetOk)


@pytest.mark.asyncio
async def test_core_db_bulk_get_revs_true(couch_setup):
    '''
    This function tests bulk get function - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    doc1_dict = {'a': 10}
    doc1 = (await post_db(uri=COUCH_URI, db=COUCH_DATABASE, doc=doc1_dict)).unwrap()

    doc2_dict = {'b': 20}
    doc2 = (await post_db(uri=COUCH_URI, db=COUCH_DATABASE, doc=doc2_dict)).unwrap()

    docs = [{"id": doc1.id}, {"id": doc2.id}]

    docs = (await bulk_get(uri=COUCH_URI, db=COUCH_DATABASE, docs=docs, revs=True)).unwrap()
    assert isinstance(docs, BulkGetOk)
    # assert "_revisions" in docs.results[0]['docs'][0]['ok']
    assert '_revisions' in json.dumps(docs.results)


@pytest.mark.xfail(raises=ResultException)
@pytest.mark.asyncio
async def test_core_db_bulk_get_wrong_uri(couch_setup):
    '''
    This function tests bulk get function - expecting exception while given wrong database
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    docs = [{'name': 'John', 'email': 'john@example.com'}, {'name': 'Steve', 'email': 'steve@example.com'}]

    docs = (await bulk_get(uri='COUCH_URI', db=COUCH_DATABASE, docs=docs)).unwrap()


@pytest.mark.xfail(raises=ResultException)
@pytest.mark.asyncio
async def test_core_db_bulk_get_wrong_uri_(couch_setup):
    '''
    This function tests bulk get function - expecting exception while given wrong database
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    docs = [{'name': 'John', 'email': 'john@example.com'}, {'name': 'Steve', 'email': 'steve@example.com'}]

    docs = (await bulk_get(uri=COUCH_URI, db='', docs=docs)).unwrap()


@pytest.mark.xfail(raises=ResultException)
@pytest.mark.asyncio
async def test_core_db_bulk_get_wrong_database(couch_setup):
    '''
    This function tests bulk get function - expecting exception while given wrong database
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    docs = [{'name': 'John', 'email': 'john@example.com'}, {'name': 'Steve', 'email': 'steve@example.com'}]

    docs = (await bulk_get(uri=COUCH_URI, db='COUCH_DATABASE', docs=docs)).unwrap()


@pytest.mark.xfail(raises=ResultException)
@pytest.mark.asyncio
async def test_core_db_bulk_get_invalid_docs(couch_setup):
    '''
    This function tests bulk get function - expecting exception while given wrong database
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    docs = ["id"]

    docs = (await bulk_get(uri=COUCH_URI, db=COUCH_DATABASE, docs=docs)).unwrap()
