import pytest

from thresult import ResultException
from thcouch.core.db import HeadDbOk, head_db


@pytest.mark.asyncio
async def test_core_db_head_existing_db(couch_setup):
    '''
    This function tests head existing db - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    # head database
    head_ = (await head_db(uri=COUCH_URI, db=COUCH_DATABASE)).unwrap()
    assert isinstance(head_, HeadDbOk)


@pytest.mark.asyncio
async def test_core_db_head_non_existing_db(couch_setup):
    '''
    This function tests head - expected exception - non existing db
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    # head database
    head_ = (await head_db(uri=COUCH_URI, db='COUCH_DATABASE')).unwrap()
    assert isinstance(head_, HeadDbOk)


@pytest.mark.xfail(raises=ResultException)
@pytest.mark.asyncio
async def test_core_db_head_fails_wrong_uri(couch_setup):
    '''
    This function tests head db function when wrong uri passed - expected exception
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    head_ = (await head_db(uri='COUCH_URI', db=COUCH_DATABASE)).unwrap()


@pytest.mark.xfail(raises=ResultException)
@pytest.mark.asyncio
async def test_core_db_head_db_param_empty_strig(couch_setup):
    '''
    This function tests head db function when db param empty string passed - expected exception
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    head_ = (await head_db(uri=COUCH_URI, db='')).unwrap()
