import pytest

from thresult import ResultException

from thcouch.core.db import PutDbOk, put_db


@pytest.mark.asyncio
async def test_core_db_put_ok(couch_setup):
    '''
    This function tests put db function for creating the db - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    test_create_db_name = 'test_create_db_ok_0'

    res = (await put_db(uri=COUCH_URI, db=test_create_db_name)).unwrap()
    assert isinstance(res, PutDbOk)


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException)
async def test_core_db_put_invalid_dbname_param(couch_setup):
    '''
    This function tests put db function fails when invalid db name is passed - exception expected
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    test_create_db_name = '_invalid_db_name'

    res = (await put_db(uri=COUCH_URI, db=test_create_db_name)).unwrap()


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException)
async def test_core_db_put_wrong_uri(couch_setup):
    '''
    This function tests put db function fails when wrong uri is passed - exception expected
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    res = (await put_db(uri='COUCH_URI', db='test_create_db_wrong_uri_db')).unwrap()


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException)
async def test_core_db_put_when_db_exists_should_fail(couch_setup):
    '''
    This function tests put db function when db already exists - exception expected
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    test_create_db_name = 'test-create-db-existing-0'

    res = (await put_db(uri=COUCH_URI, db=test_create_db_name)).unwrap()
    res = (await put_db(uri=COUCH_URI, db=test_create_db_name)).unwrap()
