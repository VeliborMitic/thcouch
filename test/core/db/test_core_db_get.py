import pytest

from thresult import ResultException

from thcouch.core.db import GetDbOk, get_db


@pytest.mark.asyncio
async def test_core_db_get_ok(couch_setup):
    '''
    This function tests get db function for get db - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    db = (await get_db(uri=COUCH_URI, db=COUCH_DATABASE)).unwrap()
    
    assert isinstance(db, GetDbOk)


@pytest.mark.xfail(raises=ResultException)
@pytest.mark.asyncio
async def test_core_db_get_fails_non_existing_db_name_param(couch_setup):
    '''
    This function tests get db function for get the db with non existing db name parameter passed - expected exception - 404 Not Found
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    
    db = (await get_db(uri=COUCH_URI, db='COUCH_DATABASE')).unwrap()


@pytest.mark.xfail(raises=ResultException)
@pytest.mark.asyncio
async def test_core_db_get_fails_wrong_uri(couch_setup):
    '''
    This function tests get db function for get the db with wrong uri parameter passed - expected exception
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    
    db = (await get_db(uri='COUCH_URI', db=COUCH_DATABASE)).unwrap()


@pytest.mark.xfail(raises=ResultException)
@pytest.mark.asyncio
async def test_core_db_get_fails_empty_string_db_name_param(couch_setup):
    '''
    This function tests get db function for get the db with empty string db name parameter passed - expected exception
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    
    db = (await get_db(uri=COUCH_URI, db=' ')).unwrap()


@pytest.mark.xfail(raises=ResultException)
@pytest.mark.asyncio
async def test_core_db_get_fails_invalid_format_db_name_param(couch_setup):
    '''
    This function tests get db function for get the db with invalid formated db name parameter passed - expected exception
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    
    db = (await get_db(uri=COUCH_URI, db='???')).unwrap()


@pytest.mark.xfail(raises=ResultException)
@pytest.mark.asyncio
async def test_core_db_get__db_name_param_empty_string(couch_setup):
    '''
    This function tests get db function when db param empty string passed - expected exception
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    db = (await get_db(uri=COUCH_URI, db='')).unwrap()
