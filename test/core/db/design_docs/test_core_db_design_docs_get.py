import pytest

from thresult import ResultException

from thcouch.core.db.design_docs import GetDesignDocsOk, get_design_docs


@pytest.mark.asyncio
async def test_core_db_design_docs_get(couch_setup):
    '''
    This function tests design docs get function - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    design_docs = (await get_design_docs(uri=COUCH_URI, db=COUCH_DATABASE)).unwrap()
    assert isinstance(design_docs, GetDesignDocsOk)


@pytest.mark.xfail(raises=ResultException)
@pytest.mark.asyncio
async def test_core_db_design_docs_get_wrong_uri(couch_setup):
    '''
    This function tests design docs get function - expecting exception while given wrong uri
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    design_docs = (await get_design_docs(uri='COUCH_URI', db=COUCH_DATABASE)).unwrap()


@pytest.mark.xfail(raises=ResultException)
@pytest.mark.asyncio
async def test_core_db_design_docs_get_wrong_db(couch_setup):
    '''
    This function tests design docs get function - wrong database
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    design_docs = (await get_design_docs(uri=COUCH_URI, db='COUCH_DATABASE')).unwrap()
        
     
@pytest.mark.xfail(raises=ResultException)   
@pytest.mark.asyncio
async def test_core_db_design_docs_get_wrong_conflicts(couch_setup):
    '''
    This function tests design docs get function - expecting exception while given wrong value for conflicts
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    design_docs = (await get_design_docs(uri=COUCH_URI, db='COUCH_DATABASE', conflicts='wrong_value')).unwrap()
        
     
@pytest.mark.xfail(raises=ResultException)    
@pytest.mark.asyncio
async def test_core_db_design_docs_get_wrong_descending(couch_setup):
    '''
    This function tests design docs get function - wrong descending
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    design_docs = (await get_design_docs(uri=COUCH_URI, db='COUCH_DATABASE', descending='wrong_value')).unwrap()
        
    
@pytest.mark.xfail(raises=ResultException)    
@pytest.mark.asyncio
async def test_core_db_design_docs_get_wrong_end_key(couch_setup):
    '''
    This function tests design docs get function - expecting exception while given wrong end_key
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    design_docs = (await get_design_docs(uri=COUCH_URI, db='COUCH_DATABASE', end_key='wrong_value')).unwrap()
        
   
@pytest.mark.xfail(raises=ResultException)     
@pytest.mark.asyncio
async def test_core_db_design_docs_get_wrong_inclusive_end(couch_setup):
    '''
    This function tests design docs get function - expecting exception while given wrong inclusive_end
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    design_docs = (await get_design_docs(uri=COUCH_URI, db='COUCH_DATABASE', inclusive_end='wrong_value')).unwrap()
        
    
@pytest.mark.xfail(raises=ResultException)      
@pytest.mark.asyncio
async def test_core_db_design_docs_get_wrong_key(couch_setup):
    '''
    This function tests design docs get function - expecting exception while given wrong key
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    design_docs = (await get_design_docs(uri=COUCH_URI, db='COUCH_DATABASE', key='wrong_value')).unwrap()
        
 
@pytest.mark.xfail(raises=ResultException)         
@pytest.mark.asyncio
async def test_core_db_design_docs_get_wrong_keys(couch_setup):
    '''
    This function tests design docs get function - expecting exception while given wrong keys
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    design_docs = (await get_design_docs(uri=COUCH_URI, db='COUCH_DATABASE', keys='wrong_value')).unwrap()
        
   
@pytest.mark.xfail(raises=ResultException)       
@pytest.mark.asyncio
async def test_core_db_design_docs_get_wrong_end_key_doc_id(couch_setup):
    '''
    This function tests design docs get function - expecting exception while given wrong end_key_doc_id
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    design_docs = (await get_design_docs(uri=COUCH_URI, db='COUCH_DATABASE', end_key_doc_id='wrong_value')).unwrap()
        
        
@pytest.mark.xfail(raises=ResultException)  
@pytest.mark.asyncio
async def test_core_db_design_docs_get_wrong_limit(couch_setup):
    '''
    This function tests design docs get function - expecting exception while given wrong limit
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    design_docs = (await get_design_docs(uri=COUCH_URI, db='COUCH_DATABASE', limit='wrong_value')).unwrap()
        
        
@pytest.mark.xfail(raises=ResultException)        
@pytest.mark.asyncio
async def test_core_db_design_docs_get_wrong_skip(couch_setup):
    '''
    This function tests design docs get function - expecting exception while given wrong skip
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    design_docs = (await get_design_docs(uri=COUCH_URI, db='COUCH_DATABASE', skip='wrong_value')).unwrap()
        
 
@pytest.mark.xfail(raises=ResultException)         
@pytest.mark.asyncio
async def test_core_db_design_docs_get_wrong_start_key(couch_setup):
    '''
    This function tests design docs get function - expecting exception while given wrong start_key
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    design_docs = (await get_design_docs(uri=COUCH_URI, db='COUCH_DATABASE', start_key='wrong_value')).unwrap()
        
   
@pytest.mark.xfail(raises=ResultException)       
@pytest.mark.asyncio
async def test_core_db_design_docs_get_wrong_start_key_doc_id(couch_setup):
    '''
    This function tests design docs get function - expecting exception while given wrong start_key_doc_id
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    design_docs = (await get_design_docs(uri=COUCH_URI, db='COUCH_DATABASE', start_key_doc_id='wrong_value')).unwrap()
        
        
@pytest.mark.xfail(raises=ResultException)  
@pytest.mark.asyncio
async def test_core_db_design_docs_get_wrong_update_seq(couch_setup):
    '''
    This function tests design docs get function - expecting exception while given wrong seq
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    design_docs = (await get_design_docs(uri=COUCH_URI, db='COUCH_DATABASE', update_seq='wrong_value')).unwrap()
