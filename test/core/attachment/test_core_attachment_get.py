import pytest

from thresult import ResultException

from thcouch.core.db import post_db
from thcouch.core.attachment import PutAttachmentOk, put_attachment, GetAttachmentOk, get_attachment


@pytest.mark.asyncio
async def test_core_attachment_get(couch_setup):
    '''
    This function tests get attachment function - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    new_doc: dict = {'name': 'Steve'}

    # post_db function is called to create a document so attachment can be created for that document
    doc = (await post_db(uri=COUCH_URI, db=COUCH_DATABASE, doc=new_doc)).unwrap()
    id = doc.id
    rev = doc.rev

    file_name  = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()

    # put_attachment function is called to create a attachment so get attachment function can be called to get that attachment
    put_att = (await put_attachment(uri=COUCH_URI, db=COUCH_DATABASE, docid=id, attachment_name=file_name, rev=rev, body=content)).unwrap()
    assert isinstance(put_att, PutAttachmentOk)

    get_att = (await get_attachment(uri=COUCH_URI, db=COUCH_DATABASE, docid=id, attname=file_name)).unwrap()
    assert isinstance(get_att, GetAttachmentOk)


@pytest.mark.xfail(raises=ResultException)
@pytest.mark.asyncio
async def test_core_attachment_get_catch_exception_given_wrong_uri(couch_setup):
    '''
    This function tests get attachment function - expecting exception when given wrong uri
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    attachment = (await get_attachment(uri="COUCH_URI", db=COUCH_DATABASE, docid='id', attname='file_name')).unwrap()


@pytest.mark.xfail(raises=ResultException)
@pytest.mark.asyncio
async def test_core_attachment_get_catch_exception_given_wrong_id(couch_setup):
    '''
    This function tests get attachment function - expecting exception when given wrong id
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    attachment = (await get_attachment(uri=COUCH_URI, db=COUCH_DATABASE, docid='id', attname='file_name')).unwrap()


@pytest.mark.xfail(raises=ResultException)
@pytest.mark.asyncio
async def test_core_attachment_get_catch_exception_given_wrong_rev(couch_setup):
    '''
    This function tests get attachment function - expecting exception when given wrong rev
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    attachment = (await get_attachment(uri=COUCH_URI, db=COUCH_DATABASE, docid='id', attname='file_name', rev='error')).unwrap()


@pytest.mark.xfail(raises=ResultException)
@pytest.mark.asyncio
async def test_core_attachment_get_catch_exception_given_wrong_range(couch_setup):
    '''
    This function tests get attachment function - expecting exceptions when given wrong range
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    attachment = (await get_attachment(uri=COUCH_URI, db=COUCH_DATABASE, docid='id', attname='file_name', range='error')).unwrap()


@pytest.mark.xfail(raises=ResultException)
@pytest.mark.asyncio
async def test_core_attachment_get_attn_name_param_empty_string(couch_setup):
    '''
    This function tests get attachment function - expecting exception when attname param empty string
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    attachment = (await get_attachment(uri=COUCH_URI, db=COUCH_DATABASE, docid='id', attname='')).unwrap()


@pytest.mark.xfail(raises=ResultException)
@pytest.mark.asyncio
async def test_core_attachment_get_docid_param_empty_string(couch_setup):
    '''
    This function tests get attachment function - expecting exception when docid param empty string
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    attachment = (await get_attachment(uri=COUCH_URI, db=COUCH_DATABASE, docid='', attname='abc.txt')).unwrap()


@pytest.mark.xfail(raises=ResultException)
@pytest.mark.asyncio
async def test_core_attachment_get_db_param_empty_string(couch_setup):
    '''
    This function tests get attachment function - expecting exception when db param empty string
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    attachment = (await get_attachment(uri=COUCH_URI, db='', docid='id', attname='abc.txt')).unwrap()

