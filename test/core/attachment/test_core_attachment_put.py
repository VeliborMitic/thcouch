import pytest

from thresult import ResultException

from thcouch.core.db import post_db
from thcouch.core.attachment import PutAttachmentOk, put_attachment


@pytest.mark.asyncio
async def test_core_attachment_put(couch_setup):
    '''
    This function tests create attachment function - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    new_doc: dict = {'name': 'Steve'}
    id = None
    ok = None
    rev = None

    # post_db function is called to create a document so attachment can be created for that document
    doc = (await post_db(uri=COUCH_URI, db=COUCH_DATABASE, doc=new_doc)).unwrap()
    id = doc.id
    rev = doc.rev

    file_name  = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()

    attachment = (await put_attachment(uri=COUCH_URI, db=COUCH_DATABASE, docid=id, attachment_name=file_name, rev=rev, body=content)).unwrap()
    assert isinstance(attachment, PutAttachmentOk)


@pytest.mark.xfail(raises=ResultException)
@pytest.mark.asyncio
async def test_core_attachment_put_exception_wrong_id(couch_setup):
    '''
    This function tests create attachment function - expecting exception while given wrong id
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    new_doc: dict = {'name': 'Steve'}
    id = None
    ok = None
    rev = None

    # post_db function is called to create a document so attachment can be created for that document
    doc = (await post_db(uri=COUCH_URI, db=COUCH_DATABASE, doc=new_doc)).unwrap()
    id = doc.id
    rev = doc.rev

    file_name  = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()

    attachment = (await put_attachment(uri=COUCH_URI, db=COUCH_DATABASE, docid="test_wrong_id", attachment_name=file_name, rev=rev, body=content)).unwrap()


@pytest.mark.xfail(raises=ResultException)
@pytest.mark.asyncio
async def test_core_attachment_put_exception_wrong_uri(couch_setup):
    '''
    This function tests create attachment function - expecting exception while given wrong uri
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    new_doc: dict = {'name': 'Steve'}
    id = None
    ok = None
    rev = None

    attachment = (await put_attachment(uri='COUCH_URI', db=COUCH_DATABASE, docid="asd", attachment_name='file_name', rev='rev', body='content')).unwrap()


@pytest.mark.xfail(raises=ResultException)
@pytest.mark.asyncio
async def test_core_attachment_put_exception_wrong_db(couch_setup):
    '''
    This function tests create attachment function - expecting exception while given wrong db
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    new_doc: dict = {'name': 'Steve'}
    id = None
    ok = None
    rev = None

    # post_db function is called to create a document so attachment can be created for that document
    doc = (await post_db(uri=COUCH_URI, db=COUCH_DATABASE, doc=new_doc)).unwrap()
    id = doc.id
    rev = doc.rev

    file_name = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()

    attachment = (await put_attachment(uri=COUCH_URI, db='COUCH_DATABASE', docid=id, attachment_name=file_name, rev=rev, body=content)).unwrap()


@pytest.mark.xfail(raises=ResultException)
@pytest.mark.asyncio
async def test_core_attachment_put_exception_bad_request(couch_setup):
    '''
    This function tests create attachment function - expecting exception while given invalid params
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup

    new_doc: dict = {'name': 'Steve'}
    id = None
    ok = None
    rev = None

    # post_db function is called to create a document so attachment can be created for that document
    doc = (await post_db(uri=COUCH_URI, db=COUCH_DATABASE, doc=new_doc)).unwrap()
    id = doc.id
    rev = doc.rev

    file_name = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()

    attachment = (await put_attachment(uri=COUCH_URI, db=COUCH_DATABASE, docid=id, attachment_name='???', rev=rev, body=content)).unwrap()
