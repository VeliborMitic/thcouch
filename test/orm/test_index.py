import pytest

from thresult import ResultException
from thcouch.orm.index import CouchIndex
from thcouch.orm.client import CouchClient
from thcouch.orm.database import CouchDatabase


# create
@pytest.mark.asyncio
async def test_orm_index_create(couch_setup):
    '''
    This function tests create index - orm - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup    
    client: CouchClient = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()
        
    index: CouchIndex = CouchIndex(database=db, name='test_orm_index_create')
    
    doc: dict = {'fields': ['collection', 'test_index']}
    index_: CouchIndex = (await index.create(doc=doc)).unwrap()
    
    assert isinstance(index_, CouchIndex)


# update      
@pytest.mark.asyncio
async def test_orm_index_update(couch_setup):
    '''
    This function tests update index - orm - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup    
    client: CouchClient = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()
        
    index: CouchIndex = CouchIndex(database=db, name='test_orm_index_create')
    
    doc: dict = {'fields': ['collection', 'test_index']}
    index_: dict = (await index.create(doc=doc, name='index1')).unwrap()
        
    doc1: dict = {'fields': ['collection', 'test_index_updated']}
    index_update: CouchIndex = (await index.update(doc=doc1, name='index1')).unwrap()

    assert isinstance(index_update, CouchIndex)    


# # delete      
@pytest.mark.asyncio
async def test_orm_index_delete(couch_setup):
    '''
    This function tests deleting of the index - orm - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup    
    client: CouchClient = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()
    
    index: CouchIndex = CouchIndex(database=db, name='test_orm_index_create')
    
    doc: dict = {'fields': ['collection', 'test_index']}
    index_created: dict = (await index.create(doc=doc, name='test_index', ddoc='uuid')).unwrap()
    
    index_deleted: bool = (await index.delete(name='test_index', designdoc='uuid')).unwrap()

    assert isinstance(index_deleted, bool) 


@pytest.mark.xfail(raises=ResultException, strict=True)
@pytest.mark.asyncio
async def test_orm_index_delete_wrong_ddoc(couch_setup):
    '''
    This function tests deletes the index - expected exception while given wrong ddoc 
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup    
    client: CouchClient = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()
    
    index: CouchIndex = CouchIndex(database=db, name='test_orm_index_create')
    
    index_deleted: bool = (await index.delete(name='test_index', designdoc='uuid3')).unwrap()


@pytest.mark.asyncio
async def test_orm_index_get(couch_setup):
    '''
    This function tests gets the index - orm - success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup    
    client: CouchClient = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()
    
    index: CouchIndex = CouchIndex(database=db, name='test_orm_index_create')
    
    doc: dict = {'fields': ['collection', 'test_index']}
    index_created: dict = (await index.create(doc=doc,name='test_index', ddoc='uuid')).unwrap()
    
    indexes: list = (await index.get()).unwrap()

    assert isinstance(indexes, list)
    assert all(isinstance(elem, dict) for elem in indexes)