import pytest

from thresult import ResultException

from thcouch.orm.database import CouchDatabase
from thcouch.orm import CouchClient, CouchDocument, CouchAttachment

#
# update document
#


@pytest.mark.asyncio
async def test_orm_document_update(couch_setup):
    '''
    This function tests create/update document - expect success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    doc_created: tuple[CouchDatabase, CouchDocument] = (await db.add_document(doc={'x': 10})).unwrap()
    _, doc = doc_created

    doc_dict = {'x': 20}
    doc_updated: CouchDocument = (await doc.update(doc=doc_dict, rev=doc['_rev'])).unwrap()

    assert isinstance(doc_updated, CouchDocument)
    assert doc['_rev'] != doc_updated['_rev']
    assert doc['x'] != doc_updated['x']


@pytest.mark.asyncio
async def test_orm_document_update_batch_mode(couch_setup):
    '''
    This function tests create/update document - when batch param 'ok' passed - updates document - success
    in batch mode, response with _rev None
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    doc_created: tuple[CouchDatabase, CouchDocument] = (await db.add_document(doc={'x': 10})).unwrap()
    _, doc = doc_created

    doc_dict = {'x': 20}
    doc_updated: CouchDocument = (await doc.update(doc_dict, rev=doc['_rev'], batch='ok')).unwrap()

    assert isinstance(doc_updated, CouchDocument)
    assert doc_updated['_rev'] is None
    assert doc['x'] != doc_updated['x']


@pytest.mark.asyncio
async def test_orm_document_update_wrong_batch_param(couch_setup):
    '''
    This function tests update document - success
    when batch param other then 'ok' passed - fails to activate batch mode and
    updates document in normal mode
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    doc_created: tuple[CouchDatabase, CouchDocument] = (await db.add_document(doc={'x': 10})).unwrap()
    _, doc = doc_created

    doc_dict = {'x': 20}
    doc_updated: CouchDocument = (await doc.update(doc_dict, rev=doc['_rev'], batch='abc')).unwrap()
      
    assert isinstance(doc_updated, CouchDocument)
    assert doc['_rev'] != doc_updated['_rev']
    assert doc['x'] != doc_updated['x']


@pytest.mark.asyncio
async def test_orm_document_update_new_edits_param_false(couch_setup):
    '''
    This function tests create/update document - success
    when new_edits param set to false - updates document
    but prevents incrementing _rev    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    doc_created: tuple[CouchDatabase, CouchDocument] = (await db.add_document(doc={'x': 10})).unwrap()
    _, doc = doc_created

    doc_dict = {'x': 20}

    doc_updated: CouchDocument = (await doc.update(doc_dict, rev=doc['_rev'], new_edits=False)).unwrap()

    assert isinstance(doc_updated, CouchDocument)
    assert doc['_rev'] == doc_updated['_rev']
    assert doc['x'] != doc_updated['x']


@pytest.mark.asyncio
async def test_orm_document_update_wrong_new_edits_param(couch_setup):
    '''
    This function tests create/update document - success
    when new_edits set to other then boolean False - updates document
    in normal mode and incrementing _rev
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    doc_created: tuple[CouchDatabase, CouchDocument] = (await db.add_document(doc={'x': 10})).unwrap()
    _, doc = doc_created

    doc_dict = {'x': 20}

    doc_updated: CouchDocument = (await doc.update(doc_dict, rev=doc['_rev'], new_edits='Abcd')).unwrap()

    assert isinstance(doc_updated, CouchDocument)
    assert doc['_rev'] != doc_updated['_rev']
    assert doc['x'] != doc_updated['x']


#
# delete document
#
@pytest.mark.asyncio
async def test_orm_document_delete(couch_setup):
    '''
    This function tests delete document - expect success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    doc_created: tuple[CouchDatabase, CouchDocument] = (await db.add_document(doc={'x': 10})).unwrap()
    _, doc = doc_created

    doc_deleted:  CouchDocument = (await doc.delete()).unwrap()
      
    assert isinstance(doc_deleted, CouchDocument)
    assert doc_deleted['_deleted']


@pytest.mark.asyncio
@pytest.mark.skip(reason='Not possible to test, delete in batch mode is not working as it suppose')
async def test_orm_document_delete_batch_mode(couch_setup):
    '''
    This function tests delete document - when batch param 'ok' passed - updates document
    in batch mode, response with _rev None
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    doc_created: tuple[CouchDatabase, CouchDocument] = (await db.add_document(doc={'x': 10})).unwrap()
    _, doc = doc_created

    doc_deleted: CouchDocument = (await doc.delete(batch='ok')).unwrap()

    assert isinstance(doc_deleted, CouchDocument)
    assert doc_deleted['_rev'] is None
    assert doc_deleted['_deleted']


@pytest.mark.asyncio
async def test_orm_document_delete_wrong_batch_param(couch_setup):
    '''
    This function tests delete document - success
    when batch param 'ok' passed - updates document
    in normal mode and incrementing _rev
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    doc_created: tuple[CouchDatabase, CouchDocument] = (await db.add_document(doc={'x': 10})).unwrap()
    _, doc = doc_created

    doc_deleted: CouchDocument = (await doc.delete(batch='abc')).unwrap()

    assert isinstance(doc_deleted, CouchDocument)
    assert doc['_rev'] != doc_deleted['_rev']
    assert doc_deleted['_deleted']


#
# attachment add
#
@pytest.mark.asyncio
async def test_orm_document_add_attachment(couch_setup):
    '''
    This function tests add attachment - expect success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    doc_created: tuple[CouchDatabase, CouchDocument] = (await db.add_document(doc={'x': 10})).unwrap()
    _, doc = doc_created

    file_name  = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()

    attachment_added: tuple[CouchDocument, CouchAttachment] = (await doc.add_attachment(attachment_name=file_name,
                                                                                        body=content)).unwrap()
    doc2, att = attachment_added

    assert isinstance(doc, CouchDocument)
    assert isinstance(att, CouchAttachment)
    assert doc['_rev'] != doc2['_rev']
    assert file_name in doc2['_attachments']
 

#
# get attachment
#
@pytest.mark.asyncio
async def test_orm_document_get_attachment(couch_setup):
    '''
    This function tests get attachment - expect success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    doc_created: tuple[CouchDatabase, CouchDocument] = (await db.add_document(doc={'x': 10})).unwrap()
    _, doc = doc_created

    file_name  = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()

    att_created: tuple[CouchDocument, CouchAttachment] = (await doc.add_attachment(attachment_name=file_name,
                                                                                   body=content)).unwrap()
    doc2, _ = att_created
    
    docid = doc2['_id']
    rev = doc2['_rev']
 
    doc_attachment: CouchAttachment = (await doc2.get_attachment(docid=docid,
                                                                attachment_name=file_name,
                                                                rev=rev)).unwrap()

    assert isinstance(doc_attachment, CouchAttachment)
    assert doc_attachment['attachment_name'] == file_name


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException, strict=True)
async def test_orm_document_get_attachment_wrong_attachment_name(couch_setup):
    '''
    This function tests get attachment - expect success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    doc_created: tuple[CouchDatabase, CouchDocument] = (await db.add_document(doc={'x': 10})).unwrap()
    _, doc = doc_created

    file_name = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()

    att_created: tuple[CouchDocument, CouchAttachment] = (await doc.add_attachment(attachment_name=file_name,
                                                                                   body=content)).unwrap()
    doc2, _ = att_created

    docid = doc2['_id']
    rev = doc2['_rev']

    doc_attachment: CouchAttachment = (await doc2.get_attachment(docid=docid,
                                                                 attachment_name='aaa',
                                                                 rev=rev)).unwrap()


#
# remove attachment
#
@pytest.mark.asyncio
async def test_orm_document_remove_attachment(couch_setup):
    '''
    This function tests get attachment - expect success
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    doc_created: tuple[CouchDatabase, CouchDocument] = (await db.add_document(doc={'x': 10})).unwrap()
    _, doc = doc_created

    file_name  = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()

    att_created: tuple[CouchDocument, CouchAttachment] = (await doc.add_attachment(attachment_name=file_name,
                                                                                   body=content)).unwrap()
    doc2, _ = att_created

    doc_att_removed: CouchDocument = (await doc2.remove_attachment(file_name)).unwrap()

    assert '_attachments' in doc2
    assert '_attachments' not in doc_att_removed
    assert doc_att_removed['_rev'] != doc2['_rev']


@pytest.mark.asyncio
async def test_orm_document_remove_attachment_wrong_attachment_name(couch_setup):
    '''
    This function tests remove attachment - success
    wrong attachment name passed
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    doc_created: tuple[CouchDatabase, CouchDocument] = (await db.add_document(doc={'x': 10})).unwrap()
    _, doc = doc_created

    file_name  = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()

    att_created: tuple[CouchDocument, CouchAttachment] = (await doc.add_attachment(attachment_name=file_name, body=content)).unwrap()
    doc2, att = att_created

    # Attempt to remove non-existing attachment, wrong name passed
    att_removed: CouchDocument = (await doc2.remove_attachment('aaaaa')).unwrap()

    assert '_attachments' in doc2
    assert '_attachments' in att_removed
    assert att_removed['_rev'] != doc2['_rev']


@pytest.mark.asyncio
@pytest.mark.xfail(raises=ResultException, strict=True)
async def test_orm_document_remove_attachment_invalid_format_attachment_name(couch_setup):
    '''
    This function tests remove attachment - expected exception while given wrong attachment name
    '''
    COUCH_URI, COUCH_DATABASE, COUCH_DATABASE_URL = couch_setup
    client = CouchClient(COUCH_URI)
    db: CouchDatabase = client.database(COUCH_DATABASE).unwrap()

    doc_created: tuple[CouchDatabase, CouchDocument] = (await db.add_document(doc={'x': 10})).unwrap()
    _, doc = doc_created

    file_name  = 'abc.txt'

    with open(file_name, 'w') as f:
        f.write('This is a text')

    with open(file_name, 'rb') as reader:
        content = reader.read()

    att_created: tuple[CouchDocument, CouchAttachment] = (await doc.add_attachment(attachment_name=file_name, body=content)).unwrap()
    doc2, _ = att_created

    # Attempt to remove non-existing attachment, wrong name passed
    att_removed: CouchDocument = (await doc2.remove_attachment('_aaaaa')).unwrap()
