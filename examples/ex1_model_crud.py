import asyncio

from thcouch.core.db import put_db, delete_db
from thcouch.orm.client import CouchClient
from thcouch.orm.database import CouchDatabase
from thcouch.orm.decl.loader import CouchLoader
from thcouch.orm.decl.model import BaseModel


COUCH_URI = 'http://tangledhub:tangledhub@couchdb-test:5984'
PATH_TO_FILE = 'model_test_files_example/model-example_0.toml'


#
# database
#

# create
async def create_database(database_name):
    '''
    Creates database by given uri and db name
    
    parameters:
        uri: str,
        db: str
        
    returns:
        putDbOk: PutDbOk
    '''
    
    # create database
    (await put_db(uri = COUCH_URI, db = database_name)).unwrap()
    
    
# delete
async def delete_database(database_name):
    '''
    Deletes database by given uri and db name
    
    parameters:
        uri: str,
        db: str
        
    returns:
        deleteDbOk: DeleteDbOk
    '''
    
    # delete database
    (await delete_db(uri = COUCH_URI, db = database_name)).unwrap()


#
# loader
#

# setup
def setup(database_name):
    '''
    Loads BaseModel from configuration file
    using CouchLoader
            
    parameters:
        db: CouchDatabase,
        path: str
        
    returns:
        loader: CouchLoader
    '''
    
    # create CouchClient object
    client: CouchClient = CouchClient(COUCH_URI)
    
    # create CouchDatabase object
    db: CouchDatabase = client.database(database_name).unwrap()

    # create CouchLoader object
    loader: CouchLoader = CouchLoader(db, path = PATH_TO_FILE)
    
    return loader


#
# model
#

# instantiate
async def instantiate_model():
    '''
    - Loads BaseModel from configuration file
    using CouchLoader
    
    - Creates instance of BaseModel type 
    
    Loader:    
        parameters:
            db: CouchDatabase,
            path: str
        
    User:
        parameters:
            email: str
    '''
    
    # database name
    database_name: str = instantiate_model.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader: CouchLoader = setup(database_name)

    # load Model type from file
    User: type = loader.User

    # create User/BaseModel object
    user0: User = User(email = 'user0@example.com')
    
    # delete database
    await delete_database(database_name)


# create
async def create_model():
    '''
    - Loads BaseModel from configuration file
    using CouchLoader
    
    - Creates instance of User/BaseModel type 
    
    - Adds document into database
    
    User:
        parameters:
            email: str
         
    User.add:        
        parameters:
            user0: User
        
        returns:
            user: User
    '''
    
    # database name
    database_name = create_model.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)

    # load Model type from file
    User: type = loader.User

    # create User/BaseModel object
    user0: User = User(email = 'user0@example.com')

    # save model to database
    user1_0: User = (await User.add(user0)).unwrap()
    
    # delete database
    await delete_database(database_name)


# get
async def get_model():
    '''
    - Loads BaseModel from configuration file
    using CouchLoader
    
    - Creates instance of User/BaseModel type
    
    - Adds document into database
    
    - Gets a specific document by given docid and
    rev for latest document revision
    
    Loader:    
        parameters:
            db: CouchDatabase,
            path: str
            
    User:
        parameters:
            email: str
         
    User.add:        
        parameters:
            user0: User
        
        returns:
            user: User
            
    User.get:        
        parameters:
            docid: str,
            rev: None | str = None 
            
        returns:
            user: User   
    '''
    
    # database name
    database_name = get_model.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)

    # load Model type from file
    User: type = loader.User

    # create User/BaseModel object
    user0: User = User(email = 'user0@example.com')

    # save model to database
    user1_0: User = (await User.add(user0)).unwrap()

    # get model from database by given id and rev, if rev is not passed document will be latest
    user0_1: User = (await User.get(docid = user1_0._id, rev = user1_0._rev)).unwrap()
    
    # delete database
    await delete_database(database_name)


# all
async def get_all_model():
    '''
    - Loads BaseModel from configuration file
    using CouchLoader
    
    - Creates instance of User/BaseModel type
    
    - Adds document into database
    
    - Gets all documents from database
    
    Loader:    
        parameters:
            db: CouchDatabase,
            path: str
            
    User:
        parameters:
            email: str
         
    User.add:        
        parameters:
            user0: User
        
        returns:
            user: User
            
    User.all:        
        returns:
            users: list[User]
    '''
    
    # database name
    database_name = get_all_model.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)

    # load Model type from file
    User: type = loader.User

    # create User/BaseModel object
    user0: User = User(email = 'user0@example.com')
    
    # save model to database
    user1_0: User = (await User.add(user0)).unwrap()

    # get list of all models
    users0: list[BaseModel] = (await User.all()).unwrap()
    
    # delete database
    await delete_database(database_name)


# find
async def find_models_by_given_selector():
    '''
    - Loads BaseModel from configuration file
    using CouchLoader
    
    - Creates instance of User/BaseModel type
    
    - Adds document into database
    
    - Finds all documents from database by
    given selector/query
    
    Loader:    
        parameters:
            db: CouchDatabase,
            path: str
            
    User:
        parameters:
            email: str
         
    User.add:        
        parameters:
            user0: User
        
        returns:
            user: User
            
    User.find:   
        parameters:
            selector: dict,
            limit: None | int = None,
            skip: None | int = None,
            sort: None | list[dict | str] = None,
            fields: None | list[str] = None,
            use_index: None | (str | list[str]) = None,
            r: None | int = None,
            bookmark: None | str = None,
            update: None | bool = None,
            stable: None | bool = None
                 
        returns:
            users: tuple[list[User], str, str]
    '''
        
    # database name
    database_name = find_models_by_given_selector.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)

    # load Model type from file
    User: type = loader.User

    # create User/BaseModel object
    user0: User = User(email = 'user0@example.com')
    
    # save model to database
    user1_0: User = (await User.add(user0)).unwrap()

    # find all models by given selector/query -> returns tuple of models list, bookmark and warning
    selector: dict = {
        'usertype': 'company'
    }

    users1: tuple[list[BaseModel], str, str] = (await User.find(selector = selector)).unwrap()
    
    # delete database
    await delete_database(database_name)


# update
async def update_model():
    '''
    - Loads BaseModel from configuration file
    using CouchLoader
    
    - Creates instance of User/BaseModel type 
    
    - Adds document into database
    
    - Updates specific document from database
    by given dict with fields to be updated
    
    Loader:    
        parameters:
            db: CouchDatabase,
            path: str
            
    User:
        parameters:
            email: str
         
    User.add:        
        parameters:
            user0: User
        
        returns:
            user: User
            
    self.update:
        parameters:
            doc: dict,
            batch: None | str = None,
            new_edits: None | bool = None
                    
        returns:
            user1_1: User
    '''
    
    # database name
    database_name = update_model.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)

    # load Model type from file
    User: type = loader.User
    
    # create User/BaseModel object
    user0: User = User(email='user0@example.com')
    
    # save model to database
    user1_0: User = (await User.add(user0)).unwrap()

    updated_doc: dict = {'email': 'user@example.com', 'phone': '0000-00-00-00'}

    # update model from database
    user1_1: User = (await user1_0.update(doc = updated_doc)).unwrap()
    
    # delete database
    await delete_database(database_name)


# delete
async def delete_model():
    '''
    - Loads BaseModel from configuration file
    using CouchLoader
    
    - Creates instance of User/BaseModel type 
    
    - Adds document into database
    
    - Deletes specific document from database
    by given instance of specific document
    and batch
    
    Loader:    
        parameters:
            db: CouchDatabase,
            path: str
            
    User:
        parameters:
            email: str
         
    User.add:        
        parameters:
            user0: User
        
        returns:
            user: User
            
    User.delete:
        parameters:
            self: BaseModel,
            batch: None | str = None
                    
        returns:
            user_1_0_1: User
    '''
    
    # database name
    database_name = delete_model.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)

    # load Model type from file
    User: type = loader.User
    
    # create User/BaseModel object
    user0: User = User(email = 'user0@example.com')
    
    # save model to database
    user1_0: User = (await User.add(user0)).unwrap()

    # delete model from database
    user_1_0_1: User = (await User.delete(user1_0)).unwrap()
    
    # delete database
    await delete_database(database_name)


# bulk docs
async def bulk_docs():
    '''
    - Loads BaseModel from configuration file
    using CouchLoader
    
    - Creates instance of User/BaseModel type 
    
    - Adds document into database
    
    - Bulk docs allows you to create and update
    multiple documents at the same time within
    a single request
    
    Loader:    
        parameters:
            db: CouchDatabase,
            path: str
            
    User:
        parameters:
            email: str
         
    User.add:        
        parameters:
            user0: User
        
        returns:
            user: User
            
    User.bulk_docs:
        parameters:
            docs: list[Union[BaseModel, dict]],
            new_edits: None | bool = None)
                    
        returns:
            users_0: list[dict]
    '''
    
    # database name
    database_name = bulk_docs.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)

    # load Model type from file
    User: type = loader.User

    # create User/BaseModel object
    user0: User = User(email = 'user0@example.com')
    
    # save model to database
    user1_0: User = (await User.add(user0)).unwrap()

    doc_list: list[BaseModel] = [user1_0,{'name': 'John', 'email': 'john@example.com'}]

    # update model from database using bulk_docs function when combined list of models and dict passed
    users_0: list[dict] = (await User.bulk_docs(docs = doc_list)).unwrap()
    
    # delete database
    await delete_database(database_name)


# bulk get
async def bulk_get():
    '''
    - Loads BaseModel from configuration file
    using CouchLoader
    
    - Creates instance of User/BaseModel type 
    
    - Adds document into database
    
    - Bulk get can be called to query several documents in bulk.
    It is well suited for fetching a specific revision of documents,
    as replicators do for example, or for getting revision history
    
    Loader:    
        parameters:
            db: CouchDatabase,
            path: str
            
    User:
        parameters:
            email: str
         
    User.add:        
        parameters:
            user0: User
        
        returns:
            user: User
            
    User.bulk_get:
        parameters:
            docs: list[dict],
            revs: None | bool = None) 
                    
        returns:
            user1_1: BaseModel
    '''
    
    # database name
    database_name = bulk_get.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)

    # load Model type from file
    User: type = loader.User

    # create User/BaseModel object
    user0: User = User(email = 'user0@example.com')
    
    # save model to database
    user1_0: User = (await User.add(user0)).unwrap()

    doc_list: list[dict] = [{'id': user1_0._id, 'rev': user1_0._rev}]

    # gets list of models using bulk_get function - return list of multiple models by given dict list
    user1_1: list[User] = (await User.bulk_get(docs = doc_list)).unwrap()
    
    # delete database
    await delete_database(database_name)


# 
## BaseObject
#

# create/dict
async def create_object_instance_using_dict():
    '''
    - Loads from configuration file using CouchLoader:
        *BaseModel type
        *BaseObject type
           
    - Creates instance of Profile/BaseModel type by
    providing list of BaseObject type
    
    - Creates instance of AgentProfile/BaseObject type    
    
    Loader:    
        parameters:
            db: CouchDatabase,
            path: str
            
    Profile:
        parameters:
            agent_profile: dict
         
    AgentProfile:        
        parameters:
            x: int
    '''
    
    # database name
    database_name = create_object_instance_using_dict.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)
    
    # load Model type from file
    Profile: type = loader.Profile
     
    # load Object type from file
    AgentProfile: type = loader.AgentProfile 
    
    # Instance AgentProfile
    a0: AgentProfile = AgentProfile(x = 10)
         
    # create Profile/BaseModel object, dict as object attribute
    profile0: Profile = Profile(agent_profile = a0.asdict())
    
    # delete database
    await delete_database(database_name)


# create/list
async def create_object_instance_using_list():
    '''
    - Loads from configuration file using CouchLoader:
        *BaseModel type
        *BaseObject type
        *BaseObject type
           
    - Creates instance of Profile/BaseModel type by
    providing list of BaseObject type
    
    - Creates instance of AgentProfile/BaseObject type  
    
    - Creates instance of BrokerProfile/BaseObject type   
    
    Loader:    
        parameters:
            db: CouchDatabase,
            path: str
            
    Profile:
        parameters:
            profiles: list[BaseObject]
         
    AgentProfile:        
        parameters:
            x: int
            
    BrokerProfile:        
        parameters:
            y: float
    '''
    
    # database name
    database_name = create_object_instance_using_list.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)

    # load Model type from file
    Profile: type = loader.Profile

    # load Object type from file
    AgentProfile: type = loader.AgentProfile 
    
    # load Object type from file
    BrokerProfile: type = loader.BrokerProfile 
    
    # Instance AgentProfile
    a0: AgentProfile = AgentProfile(x = 10)

    # Instance BrokerProfile
    b0: BrokerProfile = BrokerProfile(y = 10.00)
         
    # create Profile/BaseModel object, list as object attribute
    profile0: Profile = Profile(profiles = [a0, b0])
    
    # delete database
    await delete_database(database_name)


# create/union
async def create_object_instance_union():
    '''
    - Loads from configuration file using CouchLoader:
        *BaseModel type
        *BaseObject type
        *BaseObject type
           
    - Creates instance of Profile/BaseModel type by
    providing union of BaseObject types
    
    - Creates instance of AgentProfile/BaseObject type  
    
    - Creates instance of BrokerProfile/BaseObject type   
    
    Loader:    
        parameters:
            db: CouchDatabase,
            path: str
            
    Profile:
        parameters:
            profile: AgentProfile | BrokerProfile
         
    AgentProfile:        
        parameters:
            x: int
            
    BrokerProfile:        
        parameters:
            y: float
    '''
    
    # database name
    database_name = create_object_instance_union.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)

    # load Model type from file
    Profile: type = loader.Profile

    # load Object type from file
    AgentProfile: type = loader.AgentProfile 
    
    # load Object type from file
    BrokerProfile: type = loader.BrokerProfile 
    
    # Instance AgentProfile
    a0: AgentProfile = AgentProfile(x = 10)

    # Instance BrokerProfile
    b0: BrokerProfile = BrokerProfile(y = 10.00)
         
    # create Profile/BaseModel object, union between two object types
    # given AgentProfile
    profile0: Profile = Profile(profile = a0)

    # given BrokerProfile
    profile1: Profile = Profile(profile = b0)
    
    # delete database
    await delete_database(database_name)


# create/non existing attribute
async def create_object_instance_using_non_existing_attribute():
    '''
    - Loads from configuration file using CouchLoader:
        *BaseObject type
    
    - Creates instance of AgentProfile/BaseObject type 
    by providing non existing attribute, that attribute is 
    ignored 
    
    Loader:    
        parameters:
            db: CouchDatabase,
            path: str
         
    AgentProfile:        
        parameters:
            x: int
    '''
    
    # database name
    database_name = create_object_instance_using_non_existing_attribute.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)

    # load Object type from file
    AgentProfile: type = loader.AgentProfile 
   
    # Instance AgentProfile - when given non existring attr/key 'y' that key is ignored
    a0: AgentProfile = AgentProfile(x = 10, y = 20) # returns AgentProfile object with key: x=10
    
    # delete database
    await delete_database(database_name)


async def main():
    
    # create User/BaseModel object
    await instantiate_model()
    
    # save model to database
    await create_model()
    
    # get model from database by given id and rev
    await get_model()
    
    # get list of all models
    await get_all_model()
    
    # find all models by given selector/query
    await find_models_by_given_selector()
    
    # update model from database
    await update_model()
    
    # delete model from database
    await delete_model()
    
    # update model from database using bulk_docs function
    await bulk_docs()
    
    # gets list of models using bulk_get function
    await bulk_get()
    
    # create Profile/BaseModel object
    await create_object_instance_using_dict()
    
    # create Profile/BaseModel object, list as object attribute
    await create_object_instance_using_list()
    
    # create Profile/BaseModel object, union between two object types
    await create_object_instance_union()
    
    # Instance AgentProfile - when given non existring attr/key 'y' that key is ignored
    await create_object_instance_using_non_existing_attribute()
 

asyncio.run(main())
