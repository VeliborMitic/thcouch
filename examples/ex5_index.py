import asyncio

from thcouch.core.db import put_db, delete_db
from thcouch.orm.client import CouchClient
from thcouch.orm.database import CouchDatabase
from thcouch.orm.decl.loader import CouchLoader
from thcouch.orm.decl import BaseIndex


COUCH_URI = 'http://tangledhub:tangledhub@couchdb-test:5984'
PATH_TO_FILE = 'model_test_files_example/model-example_0.toml'


#
# database
#

# create
async def create_database(database_name):
    '''
    Creates database by given uri and db name
   
    parameters:
        uri: str,
        db: str
       
    returns:
        putDbOk: PutDbOk
    '''
    
    # create database
    (await put_db(uri=COUCH_URI, db=database_name)).unwrap()
    
    
# delete
async def delete_database(COUCH_URI, database_name):
    '''
    Deletes database by given uri and db name
   
    parameters:
        uri: str,
        db: str
       
    returns:
        deleteDbOk: DeleteDbOk
    '''
    
    # delete database
    (await delete_db(uri=COUCH_URI, db=database_name)).unwrap()


#
# loader
#

# setup
def setup(database_name):
    '''
    Loads BaseModel from configuration file
    using CouchLoader
           
    parameters:
        db: CouchDatabase,
        path: str
       
    returns:
        loader: CouchLoader
    '''
    
    # create CouchClient object
    client: CouchClient = CouchClient(COUCH_URI)
    
    # create CouchDatabase object
    db: CouchDatabase = client.database(database_name).unwrap()

    # create CouchLoader object
    loader: CouchLoader = CouchLoader(db, path=PATH_TO_FILE)
    
    return loader


#
# index
#

# instantiate
async def instantiate_index():
    '''
    - Loads BaseModel from configuration file
    using CouchLoader
   
    - Creates instance of BaseIndex type
    by providing id, name, fields and type
   
    Loader:    
        parameters:
            db: CouchDatabase,
            path: str
       
    UserIndex:
        parameters:
            id: str,
            name: str,
            fields: str,
            type: str    
    '''
    
    # database name
    database_name = instantiate_index.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)

    # load Index type from file 
    UserIndex: type = loader.UserIndex_usertype_email
        
    # create index/BaseIndex type object 
    index1 = UserIndex(id='id', name='name', fields='fields', type='type')
    
    # delete database
    await delete_database(COUCH_URI, database_name)


# create
async def create_index():
    '''
    - Loads BaseModel from configuration file
    using CouchLoader
   
    - Creates instance of BaseIndex type
    by providing id, name, fields and type
   
    - Saves BaseIndex into database by providing
    ddoc (id), partial_filter_selector and partitioned
   
    Loader:    
        parameters:
            db: CouchDatabase,
            path: str
       
    UserIndex:
        parameters:
            id: str,
            name: str,
            fields: str,
            type: str
           
    create:
        parameters:
            ddoc: Optional[str] = None,
            partial_filter_selector: Optional[dict] = None,
            partitioned: Optional[bool] = None  
           
        returns:
            index1: BaseIndex
    '''
    
    # database name
    database_name = create_index.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)

    # load Index type from file 
    UserIndex: type = loader.UserIndex_usertype_email
        
    # saving index/BaseIndex object into database 
    index1 = (await UserIndex.create(ddoc='ddoc')).unwrap()
    
    # delete database
    await delete_database(COUCH_URI, database_name)


# get
async def get_index():
    '''
    - Loads BaseModel from configuration file
    using CouchLoader
   
    - Creates instance of BaseIndex type
    by providing id, name, fields and type
   
    - Saves BaseIndex into database by providing
    ddoc (id), partial_filter_selector and partitioned
   
    - Gets list of all BaseIndexes
   
    Loader:    
        parameters:
            db: CouchDatabase,
            path: str
       
    UserIndex:
        parameters:
            id: str,
            name: str,
            fields: str,
            type: str
           
    create:
        parameters:
            ddoc: Optional[str] = None,
            partial_filter_selector: Optional[dict] = None,
            partitioned: Optional[bool] = None
           
        returns:
            index1: BaseIndex
           
    get:
        returns:
            index_list: list[BaseIndex]
    '''
    
    # database name
    database_name = get_index.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)
    
    # load Index type from file 
    UserIndex: type = loader.UserIndex_usertype_email
        
    # saving index/BaseIndex object into database 
    index1 = (await UserIndex.create(ddoc='ddoc')).unwrap()

    # getting list of all indexes from database 
    index_list: list[BaseIndex] = (await UserIndex.get()).unwrap()
    
    # delete database
    await delete_database(COUCH_URI, database_name)


# update
async def update_index():
    '''
    - Loads BaseModel from configuration file
    using CouchLoader
   
    - Creates instance of BaseIndex type
    by providing id, name, fields and type
   
    - Saves BaseIndex into database by providing
    ddoc (id), partial_filter_selector and partitioned
   
    - Update BaseIndex by providing ddoc, partial_filter_selector
    and partitioned
   
    Loader:    
        parameters:
            db: CouchDatabase,
            path: str
       
    UserIndex:
        parameters:
            id: str,
            name: str,
            fields: str,
            type: str
           
    create:
        parameters:
            ddoc: Optional[str] = None,
            partial_filter_selector: Optional[dict] = None,
            partitioned: Optional[bool] = None
           
        returns:
            index1: BaseIndex
           
    update:
        parameters:
            ddoc: Optional[str] = None,
            partial_filter_selector: Optional[dict] = None,
            partitioned: Optional[bool] = None
           
        returns:
            index1: BaseIndex  
    '''
    
    # database name
    database_name = update_index.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)
    
    # load Index type from file 
    UserIndex: type = loader.UserIndex_usertype_email
        
    # saving index/BaseIndex object into database 
    (await UserIndex.create(ddoc='ddoc')).unwrap()

    UserIndex.fields = ['email']
    
    # updating index from database 
    updated_index = (await UserIndex.update(ddoc='ddoc')).unwrap()
    
    # delete database
    await delete_database(COUCH_URI, database_name)


# delete
async def delete_index():
    '''
    - Loads BaseModel from configuration file
    using CouchLoader
   
    - Creates instance of BaseIndex type
    by providing id, name, fields and type
   
    - Saves BaseIndex into database by providing
    ddoc (id), partial_filter_selector and partitioned
   
    - deletes BaseIndex from database by providing designdoc (id)
   
    Loader:    
        parameters:
            db: CouchDatabase,
            path: str
       
    UserIndex:
        parameters:
            id: str,
            name: str,
            fields: str,
            type: str
           
    create:
        parameters:
            ddoc: Optional[str] = None,
            partial_filter_selector: Optional[dict] = None,
            partitioned: Optional[bool] = None
           
        returns:
            index1: BaseIndex
           
    delete:
        parameters:
            designdoc: Optional[str]
           
        returns:
            index_deleted: bool  
    '''
    
    # database name
    database_name = delete_index.__name__
    
    # create database
    await create_database(database_name)
    
    # setup loader
    loader = setup(database_name)

    # load Index type from file 
    UserIndex: type = loader.UserIndex_usertype_email
        
    # saving index/BaseIndex object into database 
    (await UserIndex.create(ddoc='ddoc')).unwrap()

    # deleting index from database 
    deleted_index: bool = (await UserIndex.delete(designdoc='ddoc')).unwrap()
    
    # delete database
    await delete_database(COUCH_URI, database_name)
    
    
async def main():
    
    # create index/BaseIndex type object
    await instantiate_index()
    
    # saving index/BaseIndex object into database
    await create_index()
    
     # getting list of all indexes from database
    await get_index()
    
    # updating index from database
    await update_index()
    
    # deleting index from database
    await delete_index()
        
    
asyncio.run(main())
